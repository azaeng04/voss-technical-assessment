
module.exports = {
    concurrency: 100,
    browser: [
        { width: 1024, height: 768, name: 'chrome' },
        { width: 1024, height: 768, name: 'firefox' }
    ]
}
