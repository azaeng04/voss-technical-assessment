# VOSS UI Automation Assessment

> Requirements
This project assumes you have the following already installed on your machine:

 - Git
 - Node JS 12 and up
 - Docker
 - Docker Compose

> View & Execute Tests
To execute the tests:
Docker-compose with the cypress images have been failing me so I will be using normal npm instead

If you would like to give running Cypress a try using docker-compose.
There is a bit of setup that needs to happen
* macOS
    - https://sourabhbajaj.com/blog/2017/02/07/gui-applications-docker-mac/
* Linux
    - You should have a fresh copy of Linux (I used Xubuntu 18.04)
    - Pull down the repo: `git clone git@gitlab.com:azaeng04/voss-technical-assessment.git`
    - Navigate inside the folder: `cd voss-technical-assessment`
    - Run `xhost +` in your terminal (This allows front end apps coming from Docker to run)
    - Perform an npm ci (installs faster) `docker-compose run --rm tests npm ci`
    - Execute the tests on Chrome: `docker-compose run --rm tests test`
    - A report of the run will be displayed on screen

* Windows
    - https://dev.to/darksmile92/run-gui-app-in-linux-docker-container-on-windows-host-4kde

**NB: I have executed the above in Linux mostly and executed it on Windows so i can't guarantee it will work on macOS **

I have tried running this on Windows, but it is ridiculously slow with Xserver
Firefox has been giving issues and I was unable to run it headlessly.

Open the Cypress runner to execute the tests:
    - Open Cypress test runner `docker run -it -v $PWD:/e2e -w /e2e cypress/included:4.5.0 run open --project .`
    - Select your browser to execute from
    - Click the test in the runner

You could even try the following on any of the above OSs:
 -  Execute tests in Firefox: `docker run -it -v $PWD:/e2e -w /e2e cypress/included:4.5.0 run -b firefox --project .`
 -  Execute tests in Chrome: `docker run -it -v $PWD:/e2e -w /e2e cypress/included:4.5.0 run -b chrome --project .`

If the above doesn't work then perform the steps below:

> Execute Tests
To execute the tests:

 1. Pull down the repo: `git clone git@gitlab.com:azaeng04/voss-technical-assessment.git`
 2. Navigate inside the folder: `cd voss-technical-assessment`
 3. Perform an npm install `npm install`
 4. Execute the tests `npm test`

 In CI you can navigate to the Cypress Dashboard that gives you an overview of all the runs completed: https://dashboard.cypress.io/projects/gq8aub (You will need a Google or Github account for this)
