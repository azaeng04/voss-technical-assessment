
import automationPage from './ultimate-qa/AutomationPage';
import fillOutFormsPage from './ultimate-qa/FillOutFormsPage';
import loginPage from './ultimate-qa/LogInPage';
import pricingPage from './ultimate-qa/PricingPage';
import coursesPage from './ultimate-qa/CoursesPage';

export {
    automationPage,
    fillOutFormsPage,
    loginPage,
    pricingPage,
    coursesPage,
}
