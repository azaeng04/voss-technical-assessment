
class LoginPage {
    navigateToLoginPage() {
        cy.visit('https://courses.ultimateqa.com/users/sign_in');
        return this;
    }

    enterInUsername(username: string) {
        cy.get(`#user\\[email\\]`).type(username);
        return this;
    }

    enterInPassword(password: string) {
        cy.get(`#user\\[password\\]`).type(password);
        return this;
    }

    clickSignInButton() {
        cy.get('[value=\'Sign in\']').click({ force: true });
        return this;
    }

    login(username: string, password: string) {
        cy.get(`[name='authenticity_token']`)
        .invoke('val')
        .then(authenticity_token => {
            cy.request({
                url: 'https://courses.ultimateqa.com/users/sign_in',
                method: 'POST',
                form: true,
                body: {
                    authenticity_token,
                    'user[email]': username,
                    'user[password]': password,
                    'user[remember_me]': false,
                }
            });
        });
        return this;
    }
}

export default new LoginPage();
