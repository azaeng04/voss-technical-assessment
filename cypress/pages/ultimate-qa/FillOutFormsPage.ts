
class FillOutFormsPage {
    navigateToFillOutFormsPage() {
        cy.visit('/filling-out-forms/');
        return this;
    }

    fillOutFirstForm(name: string, message: string) {
        FirstForm
        .enterInName(name)
        .enterInMessage(message)
        .clickSubmitButton();
        return this;
    }

    fillOutSecondForm(name: string, message: string) {
        SecondForm
        .enterInName(name)
        .enterInMessage(message)
        .enterInCaptcha()
        .clickSubmitButton();
        return this;
    }
}

class FirstForm {
    static enterInName(name: string) {
        cy.get('#et_pb_contact_name_0').type(name);
        return this;
    }

    static enterInMessage(message: string) {
        cy.get('#et_pb_contact_message_0').type('This is just a test');
        return this;
    }

    static clickSubmitButton() {
        cy.get('#et_pb_contact_form_0 .et_pb_contact_form').click({ force: true });
        return this;
    }
}

class SecondForm {
    static enterInName(name: string) {
        cy.get('#et_pb_contact_name_1').type(name);
        return this;
    }

    static enterInMessage(message: string) {
        cy.get('#et_pb_contact_message_1').type('This is just a test');
        return this;
    }

    static getCaptchaText() {
        return cy.get('#et_pb_contact_form_1 > div.et_pb_contact > form > div > div > p > span');
    }

    static enterInCaptcha() {
        this.getCaptchaText().then(element => {
            const numbers = element.text().split(' + ');
            const result = numbers[0] + numbers[1];
            cy.get('.et_pb_contact_captcha').type(result);
        });
        return this;
    }

    static clickSubmitButton() {
        cy.get('#et_pb_contact_form_0 .et_pb_contact_form').click({ force: true });
        return this;
    }
}

export default new FillOutFormsPage();
