
class AutomtionPage {
    verifyTitle(expectedTitle: string) {
        cy.title()
        .should('be.eql', expectedTitle);
        return this;
    }

    navigateToUlatimateQaAutomationPage() {
        cy.visit('/automation');
        return this;
    }

    clickTheLoginAutomationLink() {
        cy.get(`[href*='/users/sign_in']`).click({ force: true });
        return this;
    }
}

export default new AutomtionPage();
