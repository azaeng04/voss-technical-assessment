
class FakePricingPage {
    navigateToPricingPage() {
        cy.visit('/automation/fake-pricing-page/');
        return this;
    }

    clickPurchaseBasicOption() {
        cy.get('#et-boc > div > div > div.et_pb_section.et_pb_section_0.et_pb_with_background.et_section_regular.et_had_animation > div.et_pb_row.et_pb_row_1.et_pb_gutters2 > div.et_pb_column.et_pb_column_1_3.et_pb_column_2.et_pb_css_mix_blend_mode_passthrough > div > div > div > div.et_pb_button_wrapper > a')
        .click({ force: true });
    }
}

export default new FakePricingPage();
