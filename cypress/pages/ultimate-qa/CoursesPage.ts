

class CoursesPage {
    navigateToEnrollmentsPage() {
        cy.visit('https://courses.ultimateqa.com/enrollments');
        return this;
    }

    logout() {
        cy.get('a[class=\'dropdown__toggle-button\']')
        .click({ force: true })
        .get('li[class=\'dropdown__menu-item\'] a[href=\'/users/sign_out\']')
        .click({ force: true })
        return this;
    }
}

export default new CoursesPage();
