import { 
    automationPage,
    loginPage,
    fillOutFormsPage,
    pricingPage,
    coursesPage,
} from "../pages";

context('Ultimate QA', () => {
    it('can purchase a Basic plan on the Ultimate QA site', () => {
        automationPage
        .navigateToUlatimateQaAutomationPage()
        .verifyTitle('Automation Practice - Ultimate QA');

        cy.screenshot('Ultimate QA Automation Page');
        cy.percySnapshot('Ultimate QA Automation Page');

        loginPage
        .navigateToLoginPage()
        .login('test@mail.com', 'password');
        

        coursesPage
        .navigateToEnrollmentsPage();

        cy.screenshot('Ultimate QA Course Landing Page');
        cy.percySnapshot('Ultimate QA Course Landing Page');

        coursesPage.logout();

        fillOutFormsPage
        .navigateToFillOutFormsPage()
        .fillOutFirstForm('Azariah', 'This is just a test')
        .fillOutSecondForm('Azariah', 'This is just a test');

        pricingPage
        .navigateToPricingPage()
        .clickPurchaseBasicOption();
    });
})
